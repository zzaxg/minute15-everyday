//导入koa，和koa1不同的是，我们导入的是一个class，因此用大写的Koa表示
const Koa = require('koa')
// 下面，修改app.js，引入koa-bodyparser：
const bodyParser = require('koa-bodyparser');
// 导入controller middleware:
const controller = require('./controller');
//导入router， 注意require('koa-router')返回的是函数:
const router = require('koa-router')();

// 创建一个Koa对象表示web app本身
const app = new Koa()
// log request URL:
app.use(async (ctx, next) => {
    console.log(`Process ${ctx.request.method} ${ctx.request.url}...`);
    await next();
});

// 使用middleware:
// 注意以下顺序，三个顺序
//安装bodyParser,必须在router之前注册
app.use(bodyParser());
app.use(controller());
// add router middleware:安装
app.use(router.routes());

app.listen(4000);
console.log('app started at port 4000...');