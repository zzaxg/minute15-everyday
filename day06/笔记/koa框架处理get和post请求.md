## 微信公众号开发代理操作

###　环境

- mogoDB
- node.js
- 服务器
- ip地址

### 准备

mogoDB通过docker安装

node.js官网下载到本机

使用Ngrok代理ip，启动客户端成功的界面如下：

[![fVVSx0.png](https://z3.ax1x.com/2021/08/05/fVVSx0.png)](https://imgtu.com/i/fVVSx0)

[参考链接](https://www.ngrok.cc/_book/start/ngrok_auto.html)

> 代理域名 http: //vuessr.viphk. ngrok.org

### 启动一个本地的node服务(端口代理)

以官方文档为例

```javascript
const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello World');
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
```

命名为demo.js

```js
node demo
```

[![fVV0eS.png](https://z3.ax1x.com/2021/08/05/fVV0eS.png)](https://imgtu.com/i/fVV0eS)

[![fVVsij.md.png](https://z3.ax1x.com/2021/08/05/fVVsij.md.png)](https://imgtu.com/i/fVVsij)

> 本机是可以访问的

然后就可以通过这个域名访问到我们内网的服务了

浏览器输入代理域名http: //vuessr.viphk. ngrok.org

[![fVVyJs.md.png](https://z3.ax1x.com/2021/08/05/fVVyJs.md.png)](https://imgtu.com/i/fVVyJs)

### 为什么要用端口代理

[![fVVWLT.md.png](https://z3.ax1x.com/2021/08/05/fVVWLT.md.png)](https://imgtu.com/i/fVVWLT)

因为这里没有用到服务器，要把本机的服务代理出去，实现和微信的一个通讯，就要用到ngrok服务器实现。

## Node.js之web开发koa2

### node.js

> 简单的说 Node.js 就是运行在服务端的 JavaScript。
>
> Node.js 是一个基于Chrome JavaScript 运行时建立的一个平台。
>
> Node.js是一个事件驱动I/O服务端JavaScript环境，基于Google的V8引擎，V8引擎执行Javascript的速度非常快，性能非常好。

服务端是一种有针对性的服务程序。它的主要表现形式以“windows窗口程序”与“[控制台](https://baike.baidu.com/item/控制台)”为主。一般大型的服务端都是在[linux](https://baike.baidu.com/item/linux)环境下搭建。运行服务端的电脑称之为“服务器”。

#### JavaScript教程

> JavaScript 是 Web 的编程语言。
>
> 所有现代的 HTML 页面都使用 JavaScript。

#### 在Node上运行的JavaScript相比其他后端开发语言有何优势？

最大的优势是借助JavaScript天生的事件驱动机制加V8高性能引擎，使编写高性能Web服务轻而易举。

其次，JavaScript语言本身是完善的函数式语言，在前端开发时，开发人员往往写得比较随意，让人感觉JavaScript就是个“玩具语言”。但是，在Node环境下，通过模块化的JavaScript代码，加上函数式编程，并且无需考虑浏览器兼容性问题，直接使用最新的ECMAScript 6标准，可以完全满足工程上的需求。

### koa2入门

#### 实例代码(hello-koa)

app.js(启动文件)

```js
//导入koa，和koa1不同的是，我们导入的是一个class，因此用大写的Koa表示
const Koa = require('koa')

// 创建一个Koa对象表示web app本身
const app = new Koa()

//对任何请求,app将调用改异步函数处理请求；
app.use(async (ctx, next) => {
    await next();
    // 设置response的Content-Type:
    ctx.response.type = 'text/html';
     // 设置response的内容:
    ctx.response.body = '<h1>Hello, koa2!</h1>';
});

// 在端口3000监听:
app.listen(3000);
console.log('app started at port 3000...');
```

package.json(依赖包文件)

```json
{
    "name": "hello-koa2",
    "version": "1.0.0",
    "description": "Hello Koa 2 example with async",
    "main": "app.js",
    "scripts": {
        "start": "node app.js"
    },
    "keywords": [
        "koa",
        "async"
    ],
    "author": "Michael Liao",
    "license": "Apache-2.0",
    "repository": {
        "type": "git",
        "url": "https://github.com/michaelliao/learn-javascript.git"
    },
    "dependencies": {
        "koa": "2.0.0"
    }
}
```

> npm install一键安装依赖

启动项目：

1. 运行app.js
2. node app.js

#### 小结

核心代码是：

```js
app.use(async (ctx, next) => {
    await next();
    ctx.response.type = 'text/html';
    ctx.response.body = '<h1>Hello, koa2!</h1>';
});
```

每收到一个http请求，koa就会调用通过`app.use()`注册的async函数，并传入`ctx`和`next`参数。

可以观看后台监听请求发送和接收：

[![fecISO.png](https://z3.ax1x.com/2021/08/05/fecISO.png)](https://imgtu.com/i/fecISO)

[![fecolD.png](https://z3.ax1x.com/2021/08/05/fecolD.png)](https://imgtu.com/i/fecolD)

### 处理url

#### koa-router

先在`package.json`中添加依赖项：

```json
"koa-router": "7.0.0"
```

##### 处理get请求

```js
//导入koa，和koa1不同的是，我们导入的是一个class，因此用大写的Koa表示
const Koa = require('koa')

//导入router， 注意require('koa-router')返回的是函数:
const router = require('koa-router')();

// 创建一个Koa对象表示web app本身
const app = new Koa()
// log request URL:
app.use(async (ctx, next) => {
    console.log(`Process ${ctx.request.method} ${ctx.request.url}...`);
    await next();
});

// 使用router.get('/path', async fn)来注册一个GET请求。可以在请求路径中使用带变量的/hello/:name，变量可以通过ctx.params.name访问。
// 再运行app.js，我们就可以测试不同的URL：
// add url-route:
router.get('/hello/:name', async (ctx, next) => {
    var name = ctx.params.name;
    ctx.response.body = `<h1>Hello, ${name}!</h1>`;
});

router.get('/', async (ctx, next) => {
    ctx.response.body = '<h1>Index</h1>';
});

// add router middleware:安装
app.use(router.routes());

app.listen(4000);
console.log('app started at port 4000...');
```

[![fe2Dr4.png](https://z3.ax1x.com/2021/08/05/fe2Dr4.png)](https://imgtu.com/i/fe2Dr4)

[![fe2TZd.png](https://z3.ax1x.com/2021/08/05/fe2TZd.png)](https://imgtu.com/i/fe2TZd)

##### 处理post请求

> 用`router.get('/path', async fn)`处理的是get请求。如果要处理post请求，可以用`router.post('/path', async fn)`。
>
> 用post请求处理URL时，我们会遇到一个问题：post请求通常会发送一个表单，或者JSON，它作为request的body发送，但无论是Node.js提供的原始request对象，还是koa提供的request对象，都*不提供*解析request的body的功能！
>
> 所以，我们又需要引入另一个middleware来解析原始request请求，然后，把解析后的参数，绑定到`ctx.request.body`中。
>
> 需要用到**koa-bodyparser**

我们在`package.json`中添加依赖项：

```json
"koa-bodyparser": "3.2.0"
```

```js
//导入koa，和koa1不同的是，我们导入的是一个class，因此用大写的Koa表示
const Koa = require('koa')

// 下面，修改app.js，引入koa-bodyparser：
const bodyParser = require('koa-bodyparser');
//导入router， 注意require('koa-router')返回的是函数:
const router = require('koa-router')();



// 创建一个Koa对象表示web app本身
const app = new Koa()
// log request URL:
app.use(async (ctx, next) => {
    // console.log(`Process ${ctx.request.method} ${ctx.request.url}...`);
    await next();
});

//登录表单区域
//向本机发 get请求获取表单并展示
router.get('/', async (ctx, next) => {
    ctx.response.body = `<h1>Index</h1>
        <form action="/signin" method="post">
            <p>Name: <input name="name" value="koa"></p>
            <p>Password: <input name="password" type="password"></p>
            <p><input type="submit" value="Submit"></p>
        </form>`;
});

router.post('/signin', async (ctx, next) => {
    var
        name = ctx.request.body.name || '',
        password = ctx.request.body.password || '';
    console.log(`signin with name: ${name}, password: ${password}`);
    if (name === 'koa' && password === '12345') {
        ctx.response.body = `<h1>Welcome, ${name}!</h1>`;
    } else {
        ctx.response.body = `<h1>Login failed!</h1>
        <p><a href="/">Try again</a></p>`;
    }
});

//安装bodyParser,必须在router之前注册
app.use(bodyParser());
// add router middleware:安装
app.use(router.routes());

app.listen(5000);
console.log('app started at port 5000...');
```

get请求：

[![fe4VhR.png](https://z3.ax1x.com/2021/08/05/fe4VhR.png)](https://imgtu.com/i/fe4VhR)

[![fe4ujK.png](https://z3.ax1x.com/2021/08/05/fe4ujK.png)](https://imgtu.com/i/fe4ujK)

post请求：

[![fe4NgP.png](https://z3.ax1x.com/2021/08/05/fe4NgP.png)](https://imgtu.com/i/fe4NgP)

[![fe45E4.png](https://z3.ax1x.com/2021/08/05/fe45E4.png)](https://imgtu.com/i/fe45E4)

> 注意到我们用`var name = ctx.request.body.name || ''`拿到表单的`name`字段，如果该字段不存在，默认值设置为`''`。
>
> 类似的，put、delete、head请求也可以由router处理。

##### 重构

所有的URL处理函数都放到`app.js`里显得很乱，而且，每加一个URL，就需要修改`app.js`。随着URL越来越多，`app.js`就会越来越长。

如果能把URL处理函数集中到某个js文件，或者某几个js文件中就好了，然后让`app.js`自动导入所有处理URL的函数。这样，代码一分离，逻辑就显得清楚了。最好是这样：

```json
url2-koa/
|
+- .vscode/
|  |
|  +- launch.json <-- VSCode 配置文件
|
+- controllers/
|  |
|  +- login.js <-- 处理login相关URL
|  |
|  +- users.js <-- 处理用户管理相关URL
|
+- app.js <-- 使用koa的js
|
+- package.json <-- 项目描述文件
|
+- node_modules/ <-- npm安装的所有依赖包
```

于是我们把`url-koa`复制一份，重命名为`url2-koa`，准备重构这个项目。

目录结构：

[![feTYdS.png](https://z3.ax1x.com/2021/08/05/feTYdS.png)](https://imgtu.com/i/feTYdS)

idex.js定义请求

```js
var fn_index = async (ctx, next) => {
    ctx.response.body = `<h1>Index</h1>
        <form action="/signin" method="post">
            <p>Name: <input name="name" value="koa"></p>
            <p>Password: <input name="password" type="password"></p>
            <p><input type="submit" value="Submit"></p>
        </form>`;
};

var fn_signin = async (ctx, next) => {
    var
        name = ctx.request.body.name || '',
        password = ctx.request.body.password || '';
    console.log(`signin with name: ${name}, password: ${password}`);
    if (name === 'koa' && password === '12345') {
        ctx.response.body = `<h1>Welcome, ${name}!</h1>`;
    } else {
        ctx.response.body = `<h1>Login failed!</h1>
        <p><a href="/">Try again</a></p>`;
    }
};
// 这个index.js通过module.exports把两个URL处理函数暴露出来。
module.exports = {
    'GET /': fn_index,
    'POST /signin': fn_signin
};
```

controllers.js扫描所以请求整合

```js
const fs = require('fs');

function addMapping(router, mapping) {
    for (var url in mapping) {
        if (url.startsWith('GET ')) {
            var path = url.substring(4);
            router.get(path, mapping[url]);
            console.log(`register URL mapping: GET ${path}`);
        } else if (url.startsWith('POST ')) {
            var path = url.substring(5);
            router.post(path, mapping[url]);
            console.log(`register URL mapping: POST ${path}`);
        } else {
            console.log(`invalid URL: ${url}`);
        }
    }
}

function addControllers(router) {
    var files = fs.readdirSync(__dirname + '/controllers');
    var js_files = files.filter((f) => {
        return f.endsWith('.js');
    });

    for (var f of js_files) {
        console.log(`process controller: ${f}...`);
        let mapping = require(__dirname + '/controllers/' + f);
        addMapping(router, mapping);
    }
}

module.exports = function (dir) {
    let
        controllers_dir = dir || 'controllers', // 如果不传参数，扫描目录默认为'controllers'
        router = require('koa-router')();
    addControllers(router, controllers_dir);
    return router.routes();
};
```

app.js引入controllers.js，注意各包注册的顺序

```js
//导入koa，和koa1不同的是，我们导入的是一个class，因此用大写的Koa表示
const Koa = require('koa')
// 下面，修改app.js，引入koa-bodyparser：
const bodyParser = require('koa-bodyparser');
// 导入controller middleware:
const controller = require('./controller');
//导入router， 注意require('koa-router')返回的是函数:
const router = require('koa-router')();

// 创建一个Koa对象表示web app本身
const app = new Koa()
// log request URL:
app.use(async (ctx, next) => {
    console.log(`Process ${ctx.request.method} ${ctx.request.url}...`);
    await next();
});

// 使用middleware:
// 注意以下顺序，三个顺序
//安装bodyParser,必须在router之前注册
app.use(bodyParser());
app.use(controller());
// add router middleware:安装
app.use(router.routes());

app.listen(4000);
console.log('app started at port 4000...');
```

