#### node.js的web开发之操作MySQL

###　如何访问数据库

> 访问MySQL数据库只有一种方法，就是通过网络发送SQL命令，然后，MySQL服务器执行后返回结果。
>
> 对于Node.js程序，访问MySQL也是通过网络发送SQL命令给MySQL服务器。这个访问MySQL服务器的软件包通常称为MySQL驱动程序。不同的编程语言需要实现自己的驱动，MySQL官方提供了Java、.Net、Python、Node.js、C++和C的驱动程序，官方的Node.js驱动目前仅支持5.7以上版本，而我们上面使用的命令行程序实际上用的就是C驱动。

### 使用Sequelize

#### 项目目录(hello-sequelize)

[![fYD57V.png](https://z3.ax1x.com/2021/08/10/fYD57V.png)](https://imgtu.com/i/fYD57V)

#### 建表

##### docker装MySQL

这里我们拉取官方的最新版本的镜像：

```
$ docker pull mysql:latest
```

[![img](https://www.runoob.com/wp-content/uploads/2016/06/docker-mysql3.png)](https://www.runoob.com/wp-content/uploads/2016/06/docker-mysql3.png)

3、查看本地镜像

使用以下命令来查看是否已安装了 mysql：

```
$ docker images
```

[![img](https://www.runoob.com/wp-content/uploads/2016/06/docker-mysql6.png)](https://www.runoob.com/wp-content/uploads/2016/06/docker-mysql6.png)

在上图中可以看到我们已经安装了最新版本（latest）的 mysql 镜像。

4、运行容器

安装完成后，我们可以使用以下命令来运行 mysql 容器：

```
$ docker run -itd --name mysql-test -p 3306:3306 -e MYSQL_ROOT_PASSWORD=123456 mysql
```

参数说明：

- **-p 3306:3306** ：映射容器服务的 3306 端口到宿主机的 3306 端口，外部主机可以直接通过 **宿主机ip:3306** 访问到 MySQL 的服务。
- **MYSQL_ROOT_PASSWORD=123456**：设置 MySQL 服务 root 用户的密码。

[![img](https://www.runoob.com/wp-content/uploads/2016/06/docker-mysql4.png)](https://www.runoob.com/wp-content/uploads/2016/06/docker-mysql4.png)

5、安装成功

通过 **docker ps** 命令查看是否安装成功：

[![img](https://www.runoob.com/wp-content/uploads/2016/06/docker-mysql5.png)](https://www.runoob.com/wp-content/uploads/2016/06/docker-mysql5.png)

本机可以通过 root 和密码 123456 访问 MySQL 服务。

[![img](https://www.runoob.com/wp-content/uploads/2016/06/docker-mysql7.png)](https://www.runoob.com/wp-content/uploads/2016/06/docker-mysql7.png)

##### 用navicat连接建表

连接上MySQL后，以下写查询语句

```sql
//建立应该用户，用户名叫www，密码是www，host为%通配符
create user 'www'@'%' identified by 'www'; 
//给这个用户访问test数据库的全部权限
grant all privileges on test.* to 'www'@'%';
//刷新权限
flush privileges; 
```

> 可以检验：
>
> ```sql
> select * from mysql.user //查看所有用户
> ```
>
> [![fY6Q3D.png](https://z3.ax1x.com/2021/08/10/fY6Q3D.png)](https://imgtu.com/i/fY6Q3D)

建表语句：

```sql
create table pets (
    id varchar(50) not null,
    name varchar(100) not null,
    gender bool not null,
    birth varchar(10) not null,
    createdAt bigint not null,
    updatedAt bigint not null,
    version bigint not null,
    primary key (id)
) engine=innodb;
```

改密码：

```sql
ALTER USER 'www'@'%' IDENTIFIED WITH mysql_native_password BY 'www';
```

> mysql8 的坑 密码加密规则
>
> mysql8 跟mysql5的一个区别是 密码加密不用 mysql_native_password 方式， 默认用的是 caching_sha2_password。 创建好用户后，用windows系统本地开发环境 的 Navicatmysql 连接数据库， 提示密码错误， 后来查到是加密方式问题，改为5默认用的 mysql_native_password , 但没有重新改用户密码，导致代码一直连接不上， 后来重新用 mysql_native_password 方式再更改一次密码, 就搞定了。
>
> 解决mysql8 连接不上的方式:
>
> 1, 添加用户时 设定加密方式为 mysql_native_password ,
> 创建新用户: create user username identified with mysql_native_password by ‘password’;
> 修改用户: alter user username@‘localhost’ identified with mysql_native_password by ‘password’;
>
> 2, 修改mysql 的配置文件, 配置文件一般在 /etc/my.cnf , 编辑配置文件, 加一行 default_authentication_plugin=mysql_native_password ,(修改默认加密方式) 然后重启mysql . 进入mysqld 服务, 重新把需要用的账户, 把密码改一下,
> ALTER USER ‘root’@‘localhost’ IDENTIFIED BY ‘root’; //可以省去了with mysql_native_password,

#### 建立连接

`config.js`实际上是一个简单的配置文件：

```js
var config = {
    database: 'test', // 使用哪个数据库
    username: 'www', // 用户名
    password: 'www', // 口令
    host: '192.168.171.103', // 主机名
    port: 3306 // 端口号，MySQL默认3306
};

module.exports = config;
```

#### `app.js`中操作数据库了

```js
const Sequelize = require('sequelize');

const config = require('./config');

console.log('init sequelize...');

var sequelize = new Sequelize(config.database, config.username, config.password, {
    host: config.host,
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        idle: 30000
    }
});

var Pet = sequelize.define('pet', {
    id: {
        type: Sequelize.STRING(50),
        primaryKey: true
    },
    name: Sequelize.STRING(100),
    gender: Sequelize.BOOLEAN,
    birth: Sequelize.STRING(10),
    createdAt: Sequelize.BIGINT,
    updatedAt: Sequelize.BIGINT,
    version: Sequelize.BIGINT
}, {
        timestamps: false
    });

var now = Date.now();

Pet.create({
    id: 'g-' + now,
    name: 'Gaffey',
    gender: false,
    birth: '2007-07-07',
    createdAt: now,
    updatedAt: now,
    version: 0
}).then(function (p) {
    console.log('created.' + JSON.stringify(p));
}).catch(function (err) {
    console.log('failed: ' + err);
});

(async () => {
    var dog = await Pet.create({
        id: 'd-' + now,
        name: 'Odie',
        gender: false,
        birth: '2008-08-08',
        createdAt: now,
        updatedAt: now,
        version: 0
    });
    console.log('created: ' + JSON.stringify(dog));
})();

(async () => {
    var pets = await Pet.findAll({
        where: {
            name: 'Gaffey'
        }
    });
    console.log(`find ${pets.length} pets:`);
    for (let p of pets) {
        console.log(JSON.stringify(p));
        console.log('update pet...');
        p.gender = true;
        p.updatedAt = Date.now();
        p.version ++;
        await p.save();
        if (p.version === 3) {
            await p.destroy();
            console.log(`${p.name} was destroyed.`);
        }
    }
})();
```

#### 查看运行结果

```bash
node --use_strict app.js
```

[![fY5jR1.png](https://z3.ax1x.com/2021/08/10/fY5jR1.png)](https://imgtu.com/i/fY5jR1)
