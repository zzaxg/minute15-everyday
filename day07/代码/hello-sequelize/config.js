// 配置数据库映射
var config = {
    database: 'test', // 使用哪个数据库
    username: 'root', // 用户名
    password: 'root', // 口令
    host: '192.168.171.103', // 主机名
    port: 3306 // 端口号，MySQL默认3306
};

module.exports = config;
